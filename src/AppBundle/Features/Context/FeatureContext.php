<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function     яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('index'));
    }


    /**
     * @When /^я нахожусь на странице регистрации$/
     */
    public function яНахожусьНаСтраницеРегистрации()
    {
        $this->visit($this->getContainer()->get('router')->generate('fos_user_registration_register'));
    }

    /**
     * @When /^я нахожусь на странице авторизации$/
     */
    public function яНахожусьНаСтраницеАвторизации()
    {
        $this->visit($this->getContainer()->get('router')->generate('fos_user_security_login'));
    }

    /**
     * @When /^я авторизован$/
     */
    public function яАвторизован()
    {
        $this->getSession()->visit($this->locatePath('/login/check-yourOauthProvider?code=validCode'));
    }

    /**
     * @When /^я нахожусь на странице переводов/
     */
    public function яНахожусьНаСтраницеПереводов()
    {
        $this->visit($this->getContainer()->get('router')->generate('tasks'));
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
}
