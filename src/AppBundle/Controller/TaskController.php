<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;


class TaskController extends Controller
{
    /**
     * @Route("/{_locale}/tasks", name="tasks")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {

        $phrase = $this->get('translator')->trans('Phrase in russian');
        $save = $this->get('translator')->trans('Save');

        $form = $this->createFormBuilder()
            ->add('ruContent', TextType::class, ['label' => $phrase])
            ->add('save', SubmitType::class, ['label' => $save])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $task = new Task();
            $task->translate('ru')->setContent($data['ruContent']);
            $task->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);

            $task->mergeNewTranslations();
            $em->flush();
        }

        $tasks = $this->getDoctrine()->getRepository('AppBundle:Task')->findAll();

        return $this->render('@App/Task/new.html.twig', array(
            'form'=>$form->createView(),
            'tasks'=>$tasks,
            'user'=>$this->getUser(),
            'user_name'=>$this->getUser()->getName()
        ));
    }


    /**
     * @Route("/{_locale}/task/{id}/translate", requirements={"id": "\d+"}, name="task_translate")
     * @Method({"GET","HEAD","POST"})
     * @param Task $task
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function translatesAction(Task $task, Request $request, EntityManagerInterface $em)
    {

        if($request) {
            $en_task = $request->request->get('enContent');
            $fr_task = $request->request->get('frContent');
            $de_task = $request->request->get('deContent');
            $it_task = $request->request->get('itContent');
            $es_task = $request->request->get('esContent');

            if($en_task and $en_task!=''){
                /**
                 * @var TaskTranslation $en_translate
                 */
                $en_translate = $task->translate('en', false);
                $en_translate->setContent($en_task);
            }

            if($fr_task and $fr_task!=''){
                /**
                 * @var TaskTranslation $fr_translate
                 */
                $fr_translate = $task->translate('fr', false);
                $fr_translate->setContent($fr_task);
            }

            if($de_task and $de_task!=''){
                /**
                 * @var TaskTranslation $de_translate
                 */
                $de_translate = $task->translate('de', false);
                $de_translate->setContent($de_task);
            }

            if($it_task and $it_task!=''){
                /**
                 * @var TaskTranslation $it_translate
                 */
                $it_translate = $task->translate('it', false);
                $it_translate->setContent($it_task);
            }

            if($es_task and $es_task!=''){
                /**
                 * @var TaskTranslation $es_translate
                 */
                $es_translate = $task->translate('es', false);
                $es_translate->setContent($es_task);
            }


            $em->persist($task);

            $task->mergeNewTranslations();
            $em->flush();
        }

        return $this->render('@App/Task/translations.html.twig', array(
            'task'=>$task,
            'user'=>$this->getUser(),
            'user_name'=>$this->getUser()->getName()
        ));
    }



}
