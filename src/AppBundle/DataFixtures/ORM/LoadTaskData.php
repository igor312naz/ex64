<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskTranslation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTaskData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = $this->getReference('user1');
        $user2 = $this->getReference('user2');


        $task1 = new Task();
        $task1->setUser($user1);

        $manager->persist($task1);

        $task_ru = new TaskTranslation();
        $task_ru->setTranslatable($task1)
        ->setLocale('ru')
        ->setContent('Первая тестовая фраза.');
        $manager->persist($task_ru);

        $task_en = new TaskTranslation();
        $task_en->setTranslatable($task1)
        ->setLocale('en')
        ->setContent('The first test phrase.');
        $manager->persist($task_en);

        $task_fr = new TaskTranslation();
        $task_fr->setTranslatable($task1)
        ->setLocale('fr')
        ->setContent('La première phrase de test.');
        $manager->persist($task_fr);

        $task_de = new TaskTranslation();
        $task_de->setTranslatable($task1)
        ->setLocale('de')
        ->setContent('Die erste Testphrase.');
        $manager->persist($task_de);

        $task_it = new TaskTranslation();
        $task_it->setTranslatable($task1)
        ->setLocale('it')
        ->setContent('La prima frase di prova.');
        $manager->persist($task_it);

        $task_es = new TaskTranslation();
        $task_es->setTranslatable($task1)
        ->setLocale('es')
        ->setContent('La primera frase de prueba.');
        $manager->persist($task_es);



        $task2 = new Task();
        $task2->setUser($user2);

        $manager->persist($task2);

        $task_ru = new TaskTranslation();
        $task_ru->setTranslatable($task2)
            ->setLocale('ru')
            ->setContent('Вторая тестовая фраза.');
        $manager->persist($task_ru);


        $manager->flush();
    }



    public function getDependencies()

    {

        return array(

            LoadUserData::class,

        );

    }
}
